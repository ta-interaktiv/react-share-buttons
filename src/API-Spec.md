# API Specification / Example

```json
{
  "communities": [
    {
      "type": "dark",
      "count": 0,
      "article_id": "582c3705ab5c374e9c000001",
      "refresh": "03.02.2017 14:26:47",
      "cache": "false",
      "share_type_id": "570e4572ab5c373d3f0000db"
    },
    {
      "type": "facebook",
      "count": 1,
      "article_id": "582c3705ab5c374e9c000001",
      "url": "http://www.tagesanzeiger.ch/25823535",
      "refresh": "03.02.2017 14:26:47",
      "cache": "false",
      "share_type_id": "570e4572ab5c373d3f0000d8",
      "name": "Facebook",
      "title": "Das Team Trump ist jetzt komplett",
      "description": "Auf Facebook teilen"
    },
    {
      "type": "twitter",
      "count": 2,
      "article_id": "582c3705ab5c374e9c000001",
      "url": "http://www.tagesanzeiger.ch/25823535",
      "refresh": "03.02.2017 14:26:47",
      "cache": "false",
      "share_type_id": "570e4572ab5c373d3f0000d7",
      "name": "Twitter",
      "via": "tagesanzeiger",
      "title": "Das Team Trump ist jetzt komplett",
      "description": "Bei Twitter veröffentlichen"
    },
    {
      "type": "whatsapp",
      "count": 3,
      "article_id": "582c3705ab5c374e9c000001",
      "url": "http://www.tagesanzeiger.ch/25823535",
      "refresh": "03.02.2017 14:26:47",
      "cache": "false",
      "share_type_id": "570e4572ab5c373d3f0000da",
      "name": "Whatsapp",
      "title": "Das Team Trump ist jetzt komplett",
      "description": "Über Whatsapp texten"
    },
    {
      "type": "share",
      "count": 2,
      "article_id": "582c3705ab5c374e9c000001",
      "url": "mailto:?subject=Das Team Trump ist jetzt komplett&body=Wer wird Donald Trump im Weissen Haus unterstützen? Unsere Grafik präsentiert Minister und Spitzenleute der neuen Regierung. http://www.tagesanzeiger.ch/25823535",
      "refresh": "03.02.2017 14:26:47",
      "cache": "false",
      "share_type_id": "570e4572ab5c373d3f0000d9",
      "name": "Senden",
      "description": "Per Mail senden"
    },
    {
      "type": "comment",
      "count": 14,
      "article_id": "582c3705ab5c374e9c000001",
      "allow_comments_weekdays": true,
      "allow_comments_weekends": true,
      "comment_count_text": "Kommentare",
      "refresh": "03.02.2017 14:26:47",
      "cache": "false",
      "name": "Kommentare",
      "description": "Kommentar schreiben"
    }
  ]
}
```
