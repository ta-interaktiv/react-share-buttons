// @flow
/**
 * Renders a collection of social sharing buttons.
 * @module ReactShareButtons
 * @version 1.1
 */
import * as React from 'react'
import ShareItem from './shareItem'
import displayTypes from './displayTypes'
import supportedCommunities from './supportedCommunities'
import { map } from 'd3-collection'
import shareActionFactory from '@ta-interaktiv/share-url-factory'
import '../style/styles.css'
import type { Community } from '@ta-interaktiv/newsnet-api-flow-types/api/articles/article/communities/community'
import type { DisplayTypes } from './displayTypes'

// region CSS
import '@ta-interaktiv/semantic-ui/semantic/dist/components/icon.css'
import '@ta-interaktiv/semantic-ui/semantic/dist/components/button.css'
import '@ta-interaktiv/semantic-ui/semantic/dist/components/list.css'

// endregion

type clickHandlerFunction = (Community, MouseEvent) => void

export type ShareButtonsProps = {
  displayType?: DisplayTypes,
  inverted?: boolean,
  communities?: Community[],
  hashtags?: ?(string[]),
  url?: string,
  clickHandler?: clickHandlerFunction,
  className?: string,
  itemClassName?: string,
  iconClassName?: string,
  additionalFacebookShares?: number
}

/**
 * Renders social sharing buttons.
 * @param {ShareButtonsProps} props
 * @param [props.displayType=VERTICAL_BUTTONS] - The display
 * type, see {@link displayTypes}
 * @param [props.inverted=false] - For displaying the list on a dark
 * background.
 * @param [props.communities] - Community objects, as
 * provided by the External Services API service by Newsnet.
 * @param [props.hashtags] - Hashtags as an array of strings.
 * Facebook  will only use the first one.
 * @param [props.url=window.location.href] - The URL to share.
 * @param [props.clickHandler=noop] - The action that should be
 *   executed when the user clicks the link. The communities object is bound to
 *   this action, so that the type (or other infos) can be extracted from it.
 *   This design also means that in case of an API change the changes can be
 *   executed on the level of the polymorphic component.
 * @param [props.className] - Additional class names to be passed to the link
 * group.
 * @param [props.itemClassName] - Additional class names to be passed into
 * individual items.
 * @param [props.iconClassName] - In case you want to give the icons some
 * special treatment
 * @param [props.additionalFacebookShares] - In case of Facebook share loss
 * (e.g. when the canonical URL has been changed), you can provide
 * additional shares via this property.
 * @constructor
 */
export function ShareButtons ({
  displayType = displayTypes.VERTICAL_BUTTONS,
  inverted = false,
  communities = [
    {
      type: 'twitter',
      article_id: '-1',
      refresh: '-1',
      share_type_id: '-1',
      description: 'Bei Twitter veröffentlichen',
      count: 0,
      cache: 'false'
    },
    {
      type: 'facebook',
      description: 'Auf Facebook teilen',
      facebook_id: '147124108642216',
      count: 0,
      article_id: '-1',
      refresh: '-1',
      share_type_id: '-1',
      cache: 'false'
    },
    {
      type: 'whatsapp',
      description: 'Über WhatsApp texten',
      count: 0,
      article_id: '-1',
      refresh: '-1',
      share_type_id: '-1',
      cache: 'false'
    },
    {
      type: 'share',
      description: 'Per Mail senden',
      count: 0,
      article_id: '-1',
      refresh: '-1',
      share_type_id: '-1',
      cache: 'false'
    }
  ],
  hashtags,
  url = window.location.href
    .replace(window.location.search, '')
    .replace(window.location.hash, ''),
  clickHandler = function () {},
  className = '',
  itemClassName = '',
  iconClassName = '',
  additionalFacebookShares = 0
}: ShareButtonsProps): React.Element<*> {
  // Set up group class, depending on the desired display type.
  let groupClass = `ta sharing ui ${className}`

  switch (displayType) {
    case displayTypes.HORIZONTAL_ICONS:
      groupClass += ' horizontal link list'
      break

    case displayTypes.VERTICAL_BUTTONS:
      groupClass += ' vertical labeled small icon buttons'
      break

    case displayTypes.HORIZONTAL_BUTTONS:
      groupClass += ' icon basic buttons'
  }

  // Add "inverted" if necessary
  if (inverted) {
    groupClass += ' inverted'
  }

  // Prepare Communities array
  let mappedCommunities = map(communities, d => d.type)

  return (
    <div className={groupClass}>
      {supportedCommunities.map(communityType => {
        let community: Community = mappedCommunities.get(communityType)
        if (community === undefined) {
          return
        }

        let count = community.count

        // region Special cases

        // Mail has it's mail share URL already set up, so we need
        // to take that into account. Unless it has not been set up, of course
        let shareUrl = url
        if (communityType === 'share') {
          if (community.url === undefined) {
            shareUrl = `mailto:?subject=${window.document.title}&body=${url}`
          } else {
            shareUrl = community.url
          }
        }

        if (communityType === 'facebook') {
          count += additionalFacebookShares
        }

        // endregion

        // Create clone of the original function and bind the community
        // object to it, so we can refer to that higher in the hierarchy.
        let onClickHandler = clickHandler.bind(null, community)

        return (
          <ShareItem
            key={communityType}
            {...community}
            displayType={displayType}
            inverted={inverted}
            onClick={onClickHandler}
            className={`${itemClassName}`}
            iconClassName={iconClassName}
            shareUrl={shareActionFactory[communityType]({
              hashtags: hashtags,
              ...community,
              url: shareUrl
            })}
            count={count}
          />
        )
      })}
    </div>
  )
}

export default ShareButtons
