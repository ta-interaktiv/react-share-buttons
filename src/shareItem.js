// @flow
/**
 * A single share button. This is usually not exposed, but is a
 * sub-component of {@link ShareButtons}
 * @module shareItem
 * @version 1.1
 */

import * as React from 'react'
import displayTypes from './displayTypes'
import type { DisplayTypes } from './displayTypes'

/**
 * Share count number is only being shown if greater than this constant.
 * @const {number}
 */
const minimumShares: number = 30
const iconReplacement = {
  facebook: 'facebook',
  twitter: 'twitter',
  whatsapp: 'whatsapp',
  share: 'mail'
}

type ShareItemProps = {
  displayType: DisplayTypes,
  inverted: boolean,
  type: string,
  count: number,
  description: string,
  shareUrl: string,
  onClick: *,
  className: string,
  iconClassName: string
}

function shareCountFormatter (count) {
  return count >= minimumShares ? count : ''
}

/**
 * A share item is a single sharing item, consisting of a link with an icon.
 * Using display types, this item can be displayed in different ways, either
 * as a list item or as a button.
 *
 * @param {ShareItemProps} props
 */
export function ShareItem ({
  displayType,
  inverted = false,
  type,
  count,
  description,
  shareUrl,
  onClick = function () {},
  className = ''
}: ShareItemProps): React.Element<*> {
  let itemClass = `${type} ${inverted ? 'inverted' : ''} ${className} share`
  let innerText

  switch (displayType) {
    case displayTypes.HORIZONTAL_ICONS:
      itemClass = `${itemClass} item`
      innerText = shareCountFormatter(count)
      break

    case displayTypes.VERTICAL_BUTTONS:
      itemClass = `${className} ${inverted ? 'inverted' : ''} ui button share`
      innerText = description
      break

    case displayTypes.HORIZONTAL_BUTTONS:
      itemClass = `${iconReplacement[type]} ${className} ui icon button share`
      innerText = shareCountFormatter(count)
      break
  }

  return (
    <a
      href={shareUrl}
      title={description}
      className={itemClass}
      onClick={onClick}
      target='_top'
    >
      <i
        className={`${iconReplacement[type]} icon`}
        style={{
          color: className.indexOf('#') >= 0 ? className : ''
        }}
      />
      <span
        style={{
          color: className.indexOf('#') >= 0 ? className : ''
        }}
      >
        {innerText}
      </span>
    </a>
  )
}

export default ShareItem
