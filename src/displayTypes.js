// @flow
/**
 * Virtual enums – used to define the different forms a sharing button/icon
 * can take when being shown.
 * @module displayTypes
 * @enum {string}
 * @property {string} HORIZONTAL_ICONS - Share buttons displayed as horizontally aligned icons.
 * @property {string} VERTICAL_BUTTONS - Share buttons displayed as vertically stacked buttons.
 * @property {string} HORIZONTAL_BUTTONS – Share buttons displayed as
 * horizontal small grouped icons
 * @readonly
 */
export default {
  HORIZONTAL_ICONS: 'horizontal icons',
  VERTICAL_BUTTONS: 'vertical buttons',
  HORIZONTAL_BUTTONS: 'horizontal buttons'
}

export type DisplayTypes = 'horizontal icons' | 'vertical buttons' | 'horizontal buttons'
