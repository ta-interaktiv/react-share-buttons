/**
 * The communities this setup currently supports (i.e. with icons or the
 * shareActionFactory.
 * Currently supports `twitter`, `facebook`, `whatsapp` and `share`.
 * @module supportedCommunities
 */
export default ['twitter', 'facebook', 'whatsapp', 'share']
