// @flow
import React from 'react'
import { storiesOf } from '@storybook/react'
import ShareButtons, { displayTypes } from '../src/index'
import '@ta-interaktiv/semantic-ui/semantic/dist/components/reset.css'
import '@ta-interaktiv/semantic-ui/semantic/dist/components/site.css'
import '@ta-interaktiv/semantic-ui/semantic/dist/components/segment.css'

function clickTest (communityObject, reactEvent) {
  console.log(communityObject)
  console.log(reactEvent)
}

const communities = [
  {
    'type': 'dark',
    'count': 0,
    'article_id': '59511f3eab5c375a91000001',
    'refresh': '10.07.2017 10:48:14',
    'cache': 'false',
    'share_type_id': '570e4572ab5c373d3f0000db'
  },
  {
    'type': 'facebook',
    'count': 299,
    'article_id': '59511f3eab5c375a91000001',
    'url': 'http://www.tagesanzeiger.ch/21249326',
    'refresh': '10.07.2017 10:48:14',
    'cache': 'false',
    'share_type_id': '570e4572ab5c373d3f0000d8',
    'name': 'Facebook',
    'title': 'Hier finden Sie Ihr Steuerparadies',
    'description': 'Auf Facebook teilen',
    'facebook_id': '147124108642216'
  },
  {
    'type': 'twitter',
    'count': 80,
    'article_id': '59511f3eab5c375a91000001',
    'url': 'http://www.tagesanzeiger.ch/21249326',
    'refresh': '10.07.2017 10:48:14',
    'cache': 'false',
    'share_type_id': '570e4572ab5c373d3f0000d7',
    'name': 'Twitter',
    'via': 'tagesanzeiger',
    'title': 'Hier finden Sie Ihr Steuerparadies',
    'description': 'Bei Twitter veröffentlichen'
  },
  {
    'type': 'whatsapp',
    'count': 131,
    'article_id': '59511f3eab5c375a91000001',
    'url': 'http://www.tagesanzeiger.ch/21249326',
    'refresh': '10.07.2017 10:48:14',
    'cache': 'false',
    'share_type_id': '570e4572ab5c373d3f0000da',
    'name': 'Whatsapp',
    'title': 'Hier finden Sie Ihr Steuerparadies',
    'description': 'Über Whatsapp texten'
  },
  {
    'type': 'share',
    'count': 241,
    'article_id': '59511f3eab5c375a91000001',
    'url': 'mailto:?subject=Hier finden Sie Ihr Steuerparadies&body=Wo zahlen Sie mit Ihrem Einkommen am wenigsten Steuern? Finden Sie es mit unserer interaktiven Grafik heraus. Für Grossverdiener hat das Paradies gewechselt. http://www.tagesanzeiger.ch/21249326',
    'refresh': '10.07.2017 10:48:14',
    'cache': 'false',
    'share_type_id': '570e4572ab5c373d3f0000d9',
    'name': 'Senden',
    'description': 'Per Mail senden'
  }
]

storiesOf('Share Buttons', module)
  .add('Horizontal Icons', () => (
    <ShareButtons displayType={displayTypes.HORIZONTAL_ICONS} communities={communities} itemClassName='#ff0000' />
  ))
  .add('Horizontal Icons with additional Facebook Count', () => (
    <ShareButtons displayType={displayTypes.HORIZONTAL_ICONS}
      communities={communities} additionalFacebookShares={33} />
  ))
  .add('Horizontal Icons with HEX-Code as iconClassName', () => (
    <ShareButtons displayType={displayTypes.HORIZONTAL_ICONS}
      communities={communities} iconClassName='#FFF00F' />
  ))
  .add('Horizontal Icons with Semantic-UI Color as iconClassName', () => (
    <ShareButtons displayType={displayTypes.HORIZONTAL_ICONS}
      communities={communities} iconClassName='red' />
  ))
  .add('Inverted Horizontal Icons', () => (
    <ShareButtons displayType={displayTypes.HORIZONTAL_ICONS} inverted />
  ))
  .add('Vertical Buttons', () => (
    <ShareButtons displayType={displayTypes.VERTICAL_BUTTONS} />
  ))
  .add('Vertical inverted Buttons', () => (
    <ShareButtons displayType={displayTypes.VERTICAL_BUTTONS} inverted />
  ))
  .add('Vertical Buttons with class names', () => (
    <ShareButtons displayType={displayTypes.VERTICAL_BUTTONS} className='green inverted basic' />
  ))
  .add('List Buttons with class names', () => (
    <ShareButtons displayType={displayTypes.HORIZONTAL_ICONS}
      className='small divided relaxed' />
  ))
  .add('List Buttons with color as itemClassName', () => (
    <ShareButtons displayType={displayTypes.HORIZONTAL_ICONS}
      className='small divided relaxed' itemClassName='yellow' />
  ))
  .add('Buttons with hashtags and defined URL', () => (
    <ShareButtons displayType={displayTypes.VERTICAL_BUTTONS}
      hashtags={['such', 'wow']}
      url='http://interaktiv.tagesanzeiger.ch/2017/awesome-project' />
  ))
  .add('Buttons with a click function', () => (
    <ShareButtons clickHandler={clickTest} />
  ))
  .add('Breaker buttons', () => (
    <ShareButtons displayType={displayTypes.HORIZONTAL_BUTTONS} communities={communities} />
  ))
  .add('On Inverted background', () => (
    <div className='ui inverted grey segment'>
      <ShareButtons displayType={displayTypes.VERTICAL_BUTTONS}
        hashtags={['such', 'wow']}
        url='http://interaktiv.tagesanzeiger.ch/2017/awesome-project' />
    </div>
  ))
  .add('Inverted on Inverted background', () => (
    <div className='ui inverted grey segment'>
      <ShareButtons displayType={displayTypes.VERTICAL_BUTTONS}
        hashtags={['such', 'wow']}
        inverted
        url='http://interaktiv.tagesanzeiger.ch/2017/awesome-project' />
    </div>
  ))
