# SCSS

Compile your `styles.scss` using

```bash
yarn run compile-scss
```

The compiled source will be placed in `style/styles.css`.

For continued updates, use

```bash
yarn run watch-scss
```
