import {configure} from '@storybook/react'

function loadStories () {
  'use strict'

  require('../stories/index.js')
}

configure(loadStories, module)
