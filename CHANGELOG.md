<a name="1.6.16"></a>
## [1.6.16](https://gitlab.com/ta-interactive/react-share-buttons/compare/v1.6.15...v1.6.16) (2018-01-18)




<a name="1.6.15"></a>
## [1.6.15](https://gitlab.com/ta-interactive/react-share-buttons/compare/v1.6.14...v1.6.15) (2017-12-07)




<a name="1.6.14"></a>
## [1.6.14](https://gitlab.com/ta-interactive/react-share-buttons/compare/v1.6.11...v1.6.14) (2017-12-07)




<a name="1.6.0"></a>
# [1.6.0](https://gitlab.com/ta-interactive/react-share-buttons/compare/v1.5.1...v1.6.0) (2017-10-03)




<a name="1.6.13"></a>
## [1.6.13](https://gitlab.com/ta-interactive/react-share-buttons/compare/v1.6.11...v1.6.13) (2017-12-07)




<a name="1.6.0"></a>
# [1.6.0](https://gitlab.com/ta-interactive/react-share-buttons/compare/v1.5.1...v1.6.0) (2017-10-03)




<a name="1.6.12"></a>
## [1.6.12](https://gitlab.com/ta-interactive/react-share-buttons/compare/v1.6.11...v1.6.12) (2017-12-07)




<a name="1.6.0"></a>
# [1.6.0](https://gitlab.com/ta-interactive/react-share-buttons/compare/v1.5.1...v1.6.0) (2017-10-03)




<<<<<<< HEAD
<a name="1.6.0"></a>
# [1.6.0](https://gitlab.com/ta-interactive/react-share-buttons/compare/1.5.0...1.6.0) (2017-10-03)


### Chore

* Update eslint ([71b73f749e3f55b580be2bc56903770a7a7d0b8e](https://gitlab.com/ta-interactive/react-share-buttons/commit/71b73f749e3f55b580be2bc56903770a7a7d0b8e))

### Feature

* itemClassName goes through to <i> element ([75d6cd595c88af7530e24fbcdbf84677b0773083](https://gitlab.com/ta-interactive/react-share-buttons/commit/75d6cd595c88af7530e24fbcdbf84677b0773083))

=======
<a name="1.6.11"></a>
## [1.6.11](https://gitlab.com/ta-interactive/react-share-buttons/compare/v1.6.10...v1.6.11) (2017-10-18)




<a name="1.6.10"></a>
## [1.6.10](https://gitlab.com/ta-interactive/react-share-buttons/compare/v1.6.9...v1.6.10) (2017-10-18)




<a name="1.6.9"></a>
## [1.6.9](https://gitlab.com/ta-interactive/react-share-buttons/compare/v1.6.8...v1.6.9) (2017-10-18)




<a name="1.6.8"></a>
## [1.6.8](https://gitlab.com/ta-interactive/react-share-buttons/compare/v1.6.7...v1.6.8) (2017-10-18)




<a name="1.6.7"></a>
## [1.6.7](https://gitlab.com/ta-interactive/react-share-buttons/compare/v1.6.6...v1.6.7) (2017-10-18)




<a name="1.6.6"></a>
## [1.6.6](https://gitlab.com/ta-interactive/react-share-buttons/compare/v1.6.5...v1.6.6) (2017-10-18)


### Patch

* iconClassName can now be a HEX-Code and icons will appear in many differenct colors. ([c592c0c1d9756be48b6b65a84ab892889adc6b95](https://gitlab.com/ta-interactive/react-share-buttons/commit/c592c0c1d9756be48b6b65a84ab892889adc6b95))



<a name="1.6.5"></a>
## [1.6.5](https://gitlab.com/ta-interactive/react-share-buttons/compare/v1.6.4...v1.6.5) (2017-10-10)


### Fix

* Add es directory to files list ([794b635a831676b64196fb675a407f1b6b344170](https://gitlab.com/ta-interactive/react-share-buttons/commit/794b635a831676b64196fb675a407f1b6b344170))



<a name="1.6.4"></a>
## [1.6.4](https://gitlab.com/ta-interactive/react-share-buttons/compare/v1.6.3...v1.6.4) (2017-10-10)


### Tooling

* Change babel config and package.json to support ES modules and flow  ([72165d48e4ea415a8226900b06119c791f2eb0c4](https://gitlab.com/ta-interactive/react-share-buttons/commit/72165d48e4ea415a8226900b06119c791f2eb0c4)), closes [#15](https://gitlab.com/ta-interactive/react-share-buttons/issues/15)
* Remove ta-semantic-ui as dependency  ([d2ae5226b4b8d2f9d8999088185bfd73138c6175](https://gitlab.com/ta-interactive/react-share-buttons/commit/d2ae5226b4b8d2f9d8999088185bfd73138c6175)), closes [#13](https://gitlab.com/ta-interactive/react-share-buttons/issues/13)
* Upgrade React peer dependencies to allow React 16, too  ([3d0ead5df1ca3003b01d571ae5ebc92356d01d56](https://gitlab.com/ta-interactive/react-share-buttons/commit/3d0ead5df1ca3003b01d571ae5ebc92356d01d56)), closes [#14](https://gitlab.com/ta-interactive/react-share-buttons/issues/14)



<a name="1.6.3"></a>
## [1.6.3](https://gitlab.com/ta-interactive/react-share-buttons/compare/v1.6.2...v1.6.3) (2017-10-03)


### Tooling

* Re-add browser field in package.json to remove flow definition ([92f1cd0f812a624efd5d1e2418364eab7221d3b8](https://gitlab.com/ta-interactive/react-share-buttons/commit/92f1cd0f812a624efd5d1e2418364eab7221d3b8))



<a name="1.6.2"></a>
## [1.6.2](https://gitlab.com/ta-interactive/react-share-buttons/compare/v1.6.1...v1.6.2) (2017-10-03)


### Tooling

* Add script that pushes to git after publishing ([7544970c12fd8a1f2225528b2f250e433cd2cb69](https://gitlab.com/ta-interactive/react-share-buttons/commit/7544970c12fd8a1f2225528b2f250e433cd2cb69))



<a name="1.6.1"></a>
## [1.6.1](https://gitlab.com/ta-interactive/react-share-buttons/compare/v1.5.1...v1.6.1) (2017-10-03)


### Feature

* Add dedicated iconClassName ([12d1bb0cc432c02b7a2dc26bfd3003b31610ec6a](https://gitlab.com/ta-interactive/react-share-buttons/commit/12d1bb0cc432c02b7a2dc26bfd3003b31610ec6a))
* itemClassName goes through to <i> element ([75d6cd595c88af7530e24fbcdbf84677b0773083](https://gitlab.com/ta-interactive/react-share-buttons/commit/75d6cd595c88af7530e24fbcdbf84677b0773083))



<a name="1.5.1"></a>
## [1.5.1](https://gitlab.com/ta-interactive/react-share-buttons/compare/1.5.0...1.5.1) (2017-10-03)


### Chore

* Update eslint ([71b73f749e3f55b580be2bc56903770a7a7d0b8e](https://gitlab.com/ta-interactive/react-share-buttons/commit/71b73f749e3f55b580be2bc56903770a7a7d0b8e))

>>>>>>> fef281ca7892a7fb3acc7b022a45509731485444
### Fix

* Remove unused import ([b0bad02279b410eab85e27125153e51053fc24f0](https://gitlab.com/ta-interactive/react-share-buttons/commit/b0bad02279b410eab85e27125153e51053fc24f0))
* Update dependencies  ([6dd7c5d3190c87e46442496854505bc8c54148ec](https://gitlab.com/ta-interactive/react-share-buttons/commit/6dd7c5d3190c87e46442496854505bc8c54148ec)), closes [#11](https://gitlab.com/ta-interactive/react-share-buttons/issues/11)

### Tooling

* Add Jest ([270c15c0eb7b63f0af77212718cb9d6b75bf99aa](https://gitlab.com/ta-interactive/react-share-buttons/commit/270c15c0eb7b63f0af77212718cb9d6b75bf99aa))
* Add Storybook ([422bda0cead1923374f5aaf62cc95ba3df90266e](https://gitlab.com/ta-interactive/react-share-buttons/commit/422bda0cead1923374f5aaf62cc95ba3df90266e))



<a name="1.5.0"></a>
# [1.5.0](https://gitlab.com/ta-interactive/react-share-buttons/compare/1.4.0...1.5.0) (2017-08-15)


### Chore

* Added version bumping and tagging scripts. ([570a438e89acc07c84641c152dee850e1b125450](https://gitlab.com/ta-interactive/react-share-buttons/commit/570a438e89acc07c84641c152dee850e1b125450))

### Feature

* New property `additionalFacebookShares`  ([7c1d6ac309e7bf9414eb9c78e02fd2f4a1592f42](https://gitlab.com/ta-interactive/react-share-buttons/commit/7c1d6ac309e7bf9414eb9c78e02fd2f4a1592f42)), closes [#10](https://gitlab.com/ta-interactive/react-share-buttons/issues/10)



<a name="1.4.0"></a>
# [1.4.0](https://gitlab.com/ta-interactive/react-share-buttons/compare/1.3.2...1.4.0) (2017-08-03)


### Feature

* Add conventional-changelog-cli for quick changelog generation ([748b6531b8d808c866a92603c459d87f939f1493](https://gitlab.com/ta-interactive/react-share-buttons/commit/748b6531b8d808c866a92603c459d87f939f1493))
* Allow `className` and `itemClassName` props to be passed in ([a308cb6d011beda736ba6f58430f7d29b05f3996](https://gitlab.com/ta-interactive/react-share-buttons/commit/a308cb6d011beda736ba6f58430f7d29b05f3996)), closes [#9](https://gitlab.com/ta-interactive/react-share-buttons/issues/9)

### Fix

* Workaround for too bright link text colours on buttons within inverted elements ([9c705bc0f19f21f18a301883a28ca574650af214](https://gitlab.com/ta-interactive/react-share-buttons/commit/9c705bc0f19f21f18a301883a28ca574650af214))

## 1.3.2 – 2017-07-24
### Changes
- Rearranged code to ignore the API provided URL. Always uses window.location
 now, unless URL is set as a prop.

## 1.3.1 – 2017-07-24
### 🐛 Bugfixes
- Remove coloration of vertical buttons

## 1.3.0 – 2017-07-10
### ✨ New Features
- Add `HORIZONTAL_BUTTONS` variation to used to break up long articles.

## 1.2.0 – 2017-07-05
### ✨ New Features
- Share buttons support now WhatsApp and Mail sharing
- Share URL Factory is now its own package.

## 1.1.0 - 2017-04-19
### ✨ New Features
- Pass in a `clickHandler` function to do ... interesting stuff whenever a user clicks the button.

### 🐛 Bugfixes
- Fixed ES6 import/exports (💥 changes the way supporting objects need to be imported and used)

### 📝 Documentation
- Added documentation for a single share item, for community objects (as provided by Newsnet API)

### 🔨 Refactoring
- Switched to prop-types.

### 🔧 Configuration
- Switched over to [Javascript Standard Style](https://standardjs.com)

### ✅ Tests
- Added storybook story with a onClick handler.

## 1.0.2 - 2017-03-30
### ✨ New Features
- Only show share count if higher than 30.

## 1.0.1 – 2017-02-13
It totally was buggy still. It properly imports the shareButtons source file now.

## 1.0.0 – 2017-02-09
First release. Might be a bit buggy still?
